#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

require 'csv'
require 'optparse'
# require 'pry'

class String
  def numeric?
    Float(self) != nil rescue false
  end
end

def main

  options = {}
  option_parser = OptionParser.new do |opts|
    executable_name = File.basename($PROGRAM_NAME)
    opts.banner = "This script just split some chineses words and statement
from a CSV file.

Usage: #{executable_name} -i input_filename -o output_filename -f freq"

    opts.on("-i input-file",
            "input file which contains chinese words.") do |name|
      options[:input_filename] = name
    end

    opts.on("-o output-file",
            "output file which contains sorted segmented words") do |name|
      options[:output_filename] = name
    end

    opts.on("-f word-frequency",
            "specify the filter word frequency, which will filter out all words
            under this frequency.") do |name|
      options[:word_frequency] = name.to_i
    end

    opts.on("-s seperator",
            "specify the field seperator for csv files") do |name|
      options[:seperator] = name
    end

    opts.on("-I ignored_field",
            "specify the ignored field by index for csv files") do |name|
      options[:ignored_field] = name.split.map { |x| x.to_i }
    end
  end

  option_parser.parse!

  unless ARGV.empty?
    puts "ERROR: wrong command usage!"
    puts
    puts option_parser.help
  end

  input_file = File.open(options[:input_filename])
  output_file = File.open(options[:output_filename], "w+")

  words = {}

  CSV.foreach(input_file, {:col_sep => options[:seperator]}) do |col|
    col.each_with_index do |word, index|
      if word == nil || word.numeric? || word =~ /^[0-9a-zA-Z\s\-_@\.\/~\*]*$/ ||
          options[:ignored_field].member?(index) || word.length > options[:word_frequency]
        next
      else
        words[word] = words.key?(word) ? words[word] + 1 : 1
      end
    end
  end

  words.reject! do |key, value|
    value <= options[:word_frequency].to_i
  end

  high_freq_words = words.keys.sort_by! { |word| word.length }

  output_file << high_freq_words.join("\n")
end

if __FILE__ == $0
  main
end
