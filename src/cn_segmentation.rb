#!/usr/bin/env ruby

require 'optparse'
require 'rmmseg'

def main

  options = {}
  option_parser = OptionParser.new do |opts|
    executable_name = File.basename($PROGRAM_NAME)
    opts.banner = "Do chinese word segmentation to input file and output a
sorted list of words.

Usage: #{executable_name} [options] input_filename output_filename"

    opts.on("-i input-file",
            "input file which contains chinese words.") do |name|
      options[:input_filename] = name
    end

    opts.on("-o output-file",
            "output file which contains sorted segmented words") do |name|
      options[:output_filename] = name
    end
  end

  option_parser.parse!

  unless ARGV.empty?
    puts "ERROR: wrong command usages!"
    puts
    puts option_parser.help
  end

  input_file = File.open(options[:input_filename])
  output_file = File.open(options[:output_filename], "w+")

  words = []

  RMMSeg::Dictionary.load_dictionaries
  algor = RMMSeg::Algorithm.new(input_file.read)

  tok = algor.next_token
  unless tok.nil?
    words << tok.text

    loop do
      tok = algor.next_token
      break if tok.nil?
      words << tok.text
    end
  end

  words.uniq!
  words.reject! { |word| word =~ /[0-9a-zA-Z]/ }

  # binding.pry
  words.reject! do |word|
    # puts "#{word}: #{word.length}"
    # UTF-8 chinese char occupies 3 bytes
    word.length <= 3
  end

  output_file << words.join("\n")
end

if __FILE__ == $0
  main
end
