#!/usr/bin/env ruby

require 'csv'
require 'optparse'
# require 'pry'

# This function takes an input file and an output file and then build a hash
# whose key comes from the input file and values comes from output file. The
# key and value are line by line.
def build_file_hash(input_file, output_file)
  input = File.open(input_file)
  output = File.open(output_file)
  input_words = input.readlines.map! { |word| word.strip }
  output_words = output.readlines.map! { |word| word.strip }

  Hash[input_words.zip(output_words)]
end

def main
  options = {}

  option_parser = OptionParser.new do |opts|
    executable_name = File.basename($PROGRAM_NAME)
    opts.banner = "This script will replace all cn words to en in a csv file.

Usage: #{executable_name} -c cn_words -e en_words -i input_csv -o output_csv"

    opts.on("-c cn_words_file",
            "This file contains the original Chinese words") do |name|
      options[:cn_words_file] = name
    end

    opts.on("-e en_words_file",
            "This file contains the translated English words") do |name|
      options[:en_words_file] = name
    end

    opts.on("-i input_csv_file",
            "The input csv file") do |name|
      options[:input_csv_file] = name
    end

    opts.on("-o output_csv_file",
            "The output csv file") do |name|
      options[:output_csv_file] = name
    end
  end

  option_parser.parse!

  unless ARGV.empty?
    puts "ERROR: wrong command usage!"
    puts
    puts option_parser.help
  end

  translation = build_file_hash(options[:cn_words_file],
                                options[:en_words_file])

  input_csv_file = options[:input_csv_file]
  output_csv_file = options[:output_csv_file]
  CSV.open(output_csv_file, "w") do |csv|
    CSV.foreach (input_csv_file) do |col|
      col.map! do |word|
        translation[word] || word
      end
      csv << col
    end

  end

end

if __FILE__ == $0
  main
end
